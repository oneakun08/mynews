plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("kotlin-parcelize")
//    id("com.google.devtools.ksp")
    id ("kotlin-kapt")
}

android {
    namespace = "com.skaha.mynews"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.skaha.mynews"
        minSdk = 21
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        buildConfigField("String", "BASE_URL", "\"https://newsapi.org/v2/\"")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        viewBinding = true
    }
    buildFeatures {
        buildConfig = true
    }
}

dependencies {

    implementation("androidx.core:core-ktx:1.9.0")
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("com.google.android.material:material:1.11.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    implementation("androidx.datastore:datastore-preferences:1.0.0")
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")

    implementation("androidx.fragment:fragment-ktx:1.6.2")
    implementation ("androidx.activity:activity-ktx:1.8.2")

    //Koin
    implementation ("io.insert-koin:koin-core:3.1.2")
    implementation ("io.insert-koin:koin-android:3.1.2")
    implementation ("androidx.legacy:legacy-support-v4:1.0.0")

    //HTTP Inspector
    debugImplementation ("com.github.chuckerteam.chucker:library:4.0.0")
    releaseImplementation ("com.github.chuckerteam.chucker:library-no-op:3.5.2")

    //AndroidViewBinding Helper
    implementation ("com.oratakashi:AndroidViewBinding:3.19.0")

    //Swipe Refresh
    implementation ("androidx.swiperefreshlayout:swiperefreshlayout:1.1.0")
    implementation ("io.coil-kt:coil:2.5.0")

    //Loading
    implementation ("com.airbnb.android:lottie:5.2.0")

    //Coroutines
    implementation ("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.7.3")
    implementation ("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.7.3")
    implementation ("androidx.lifecycle:lifecycle-runtime-ktx:2.7.0")

    //Youtube
    implementation ("com.pierfrancescosoffritti.androidyoutubeplayer:core:12.1.0")

    // navigation
    implementation ("androidx.navigation:navigation-fragment-ktx:2.7.7")
    implementation ("androidx.navigation:navigation-ui-ktx:2.7.7")

    // Room components
    implementation ("androidx.room:room-ktx:2.4.0")
    kapt ("androidx.room:room-compiler:2.4.0")
    androidTestImplementation ("androidx.room:room-testing:2.4.0")

    //Swipe Refresh
    implementation ("androidx.swiperefreshlayout:swiperefreshlayout:1.1.0")
    implementation ("io.coil-kt:coil:2.5.0")

    //Loading
    implementation ("com.airbnb.android:lottie:5.2.0")



//    implementation ("androidx.room:room-ktx:2.6.1")
//    ksp ("androidx.room:room-compiler:2.6.1")
//    androidTestImplementation ("androidx.room:room-testing:2.6.1")

    testImplementation ("junit:junit:4.13.2")
    androidTestImplementation ("androidx.test.ext:junit:1.1.5")
    androidTestImplementation ("androidx.test.espresso:espresso-core:3.5.1")
}