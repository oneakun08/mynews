package com.skaha.mynews

import android.app.Application
import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import com.skaha.mynews.di.apiModule
import com.skaha.mynews.di.reqresModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {

    init {
        instance = this
    }

    companion object {
        private var instance: App? = null

        fun applicationContext() : Context {
            return instance!!.applicationContext
        }
    }
    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setDefaultNightMode(
            AppCompatDelegate.MODE_NIGHT_YES
        )
        startKoin {
            androidContext(applicationContext)
            modules(
                listOf(
                    apiModule,
                    reqresModule
                )
            )
        }
    }
}