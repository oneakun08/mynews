package com.skaha.mynews.data

import com.skaha.mynews.data.model.news.NewsEntity
import com.skaha.mynews.data.web.NewsApi
import com.skaha.mynews.utils.DataStoreManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import android.content.Context
import com.skaha.mynews.App

class NewsDataStore(
    private val webService: NewsApi,
) : NewsRepository {
    override fun getNews(page: Int, isNew: Boolean, category: String): Flow<List<NewsEntity>> {
        return flow {
            val news = webService.getNews("us", category, page, "")
            emit(news.articles.map { data ->
                data.toNewsEntity()
            })
            news.totalResults?.let { DataStoreManager(context = App.applicationContext()).saveTotalArticles(it) }
        }.catch {
            println("NewsDataStore: getNews: ${it.message}")
        }.flowOn(Dispatchers.IO)
    }
}