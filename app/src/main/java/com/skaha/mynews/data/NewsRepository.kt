package com.skaha.mynews.data

import com.skaha.mynews.data.model.news.NewsEntity
import kotlinx.coroutines.flow.Flow

interface NewsRepository {
    fun getNews(page: Int, isNew: Boolean, category: String): Flow<List<NewsEntity>>

}