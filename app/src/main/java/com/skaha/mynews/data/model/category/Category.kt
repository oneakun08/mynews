package com.skaha.mynews.data.model.category

data class Category (
    val category: String
)