package com.skaha.mynews.data.model.news

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DataSource (
    @SerializedName("name")
    val name: String? = null,

    @SerializedName("id")
    val id: String? = null
): Parcelable
{
    fun toDataSource(): SourceEntity {
        return SourceEntity(name, id)
    }
}