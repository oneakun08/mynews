package com.skaha.mynews.data.model.news

import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class ResponseNews (
    @SerializedName("totalResults")
    val totalResults: Int? = null,

    @SerializedName("articles")
    val articles: List<DataArticle>,

    @SerializedName("status")
    val status: String? = null
)