package com.skaha.mynews.data.model.news

data class SourceEntity (
    val name: String? = null,
    val id: Any? = null
)