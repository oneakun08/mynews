package com.skaha.mynews.data.web

import com.skaha.mynews.data.model.news.ResponseNews

class NewsApi(
    private val api : NewsApiClient
): NewsApiClient {

    override suspend fun getNews(
        country: String,
        category: String,
        page: Int,
        q: String
    ): ResponseNews {
        return api.getNews(country, category, page, q)
    }
}