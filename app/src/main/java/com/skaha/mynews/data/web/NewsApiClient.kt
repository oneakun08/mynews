package com.skaha.mynews.data.web

import com.skaha.mynews.data.model.news.ResponseNews
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApiClient {
    @GET("top-headlines")
    suspend fun getNews(
        @Query("country") country: String,
        @Query("category") category: String,
        @Query("page") page: Int,
        @Query("q") q: String,
    ): ResponseNews
}