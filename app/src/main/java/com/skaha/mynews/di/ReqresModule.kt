package com.skaha.mynews.di

import com.skaha.mynews.data.NewsDataStore
import com.skaha.mynews.data.NewsRepository
import com.skaha.mynews.ui.home.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val reqresModule = module {
    single<NewsRepository> {
        NewsDataStore(
            get()
        )
    }
    viewModel { HomeViewModel(get()) }
}