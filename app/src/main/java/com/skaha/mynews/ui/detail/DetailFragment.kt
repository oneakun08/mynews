package com.skaha.mynews.ui.detail

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.oratakashi.viewbinding.core.viewBinding
import com.skaha.mynews.R
import com.skaha.mynews.data.model.news.NewsEntity
import com.skaha.mynews.databinding.FragmentDetailBinding

class DetailFragment : Fragment() {

    private val binding: FragmentDetailBinding by viewBinding()
    private lateinit var navController: NavController
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)
        //val news = arguments?.getSerializable("dataNews") as NewsEntity
        val url = arguments?.getString("url")
        if (url != null) {
            binding.webView?.apply {
                webViewClient = WebViewClient()
                loadUrl(url)
            }
        } else {
            //back to news fragment if link to article is null
            navController.popBackStack()
        }

        binding.fabShare?.setOnClickListener {
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type="text/plain"
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Share this article...")
            shareIntent.putExtra(Intent.EXTRA_TEXT, url)
            startActivity(shareIntent)
        }
    }
}