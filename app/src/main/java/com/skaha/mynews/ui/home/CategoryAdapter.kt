package com.skaha.mynews.ui.home

import android.annotation.SuppressLint
import android.content.res.Configuration
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import com.skaha.mynews.R
import com.skaha.mynews.databinding.AdapterCategoryBinding
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch


class CategoryAdapter(
    private var category: List<String> = emptyList(),
    private val listener: ICategoryRVAdapter
) : RecyclerView.Adapter<CategoryAdapter.MyViewHolder>() {

    lateinit var binding: AdapterCategoryBinding
    private var selectedPosition = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        binding = AdapterCategoryBinding.inflate(inflater, parent, false)
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, @SuppressLint("RecyclerView") position: Int) {
        holder.bind(category[position])

        holder.itemView.setOnClickListener {

            listener.onCategoryClicked(category[position])

            if (selectedPosition == position) {
                notifyItemChanged(position)
            }

            selectedPosition = position
            notifyDataSetChanged()


        }
    }

    override fun getItemCount(): Int = category.size

    inner class MyViewHolder(private val binding: AdapterCategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("NotifyDataSetChanged")
        fun bind(article: String) {
            binding.tvCategoryItem.text = article
            when(itemView.context.resources?.configuration?.uiMode?.and(Configuration.UI_MODE_NIGHT_MASK)) {
                Configuration.UI_MODE_NIGHT_YES -> {
                    if (selectedPosition == position) {
                        binding.tvCategoryItem.setTextColor(ContextCompat.getColor(itemView.context, R.color.white))
                        binding.tvCategoryItem.background = ContextCompat.getDrawable(itemView.context, R.drawable.category_item_selected_bg)
                    } else {
                        binding.tvCategoryItem.setTextColor(ContextCompat.getColor(itemView.context, R.color.white_alpha_60))
                        binding.tvCategoryItem.background = ContextCompat.getDrawable(itemView.context, R.drawable.category_item_unselected_dark_bg)
                    }
                }
                Configuration.UI_MODE_NIGHT_NO -> {
                    if (selectedPosition == position) {
                        binding.tvCategoryItem.setTextColor(ContextCompat.getColor(itemView.context, R.color.white))
                        binding.tvCategoryItem.background = ContextCompat.getDrawable(itemView.context, R.drawable.category_item_selected_bg)
                    } else {
                        binding.tvCategoryItem.setTextColor(ContextCompat.getColor(itemView.context, R.color.black_alpha_70))
                        binding.tvCategoryItem.background = ContextCompat.getDrawable(itemView.context, R.drawable.category_item_unselected_bg)
                    }
                }
                Configuration.UI_MODE_NIGHT_UNDEFINED -> {
                    if (selectedPosition == position) {
                        binding.tvCategoryItem.setTextColor(ContextCompat.getColor(itemView.context, R.color.white))
                        binding.tvCategoryItem.background = ContextCompat.getDrawable(itemView.context, R.drawable.category_item_selected_bg)
                    } else {
                        binding.tvCategoryItem.setTextColor(ContextCompat.getColor(itemView.context, R.color.black_alpha_70))
                        binding.tvCategoryItem.background = ContextCompat.getDrawable(itemView.context, R.drawable.category_item_unselected_bg)
                    }
                }

            }
        }
    }
    interface ICategoryRVAdapter{
        fun onCategoryClicked(category: String)
    }

}