package com.skaha.mynews.ui.home

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import androidx.lifecycle.asLiveData
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.oratakashi.viewbinding.core.binding.fragment.viewBinding
import com.oratakashi.viewbinding.core.tools.State
import com.oratakashi.viewbinding.core.tools.toast
import com.skaha.mynews.R
import com.skaha.mynews.databinding.FragmentHomeBinding
import com.skaha.mynews.utils.Constant
import com.skaha.mynews.utils.DataStoreManager
import com.skaha.mynews.utils.OnLoadMoreListener
import com.skaha.mynews.utils.RVLoadMore
import com.skaha.mynews.utils.extention.initRecycler
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : Fragment(), CategoryAdapter.ICategoryRVAdapter {

    private val binding: FragmentHomeBinding by viewBinding()
    private val viewModel: HomeViewModel by viewModel()
    private val newsAdapter: NewsAdapter by lazy {
        NewsAdapter()
    }
    private var itemsDisplayed = 0
    private var totalArticles: Int? = null
    private var rvLoadMore: RVLoadMore = RVLoadMore()
    private val categories =
        listOf("General", "Business", "Entertainment", "Sports", "Health", "Science", "Technology")
    private val categoryAdapter: CategoryAdapter by lazy { CategoryAdapter(categories, this) }
    private var currentCategory = "General"
    private lateinit var navController: NavController
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        return binding.root
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        darkMode()
        setUpCategoriesRecyclerView()
        setUpNewsRecyclerView()
        initAction()
        initObserver()
    }

    private fun initAction() {
        viewModel.getNews(1, true, currentCategory)
    }

    private fun initObserver() {
        with(binding) {
            viewModel.news.observe(viewLifecycleOwner) {
                when (it) {
                    is State.Loading -> {
                        toast("Loading")
                    }

                    is State.Success -> {
                        progressBar.visibility = View.GONE
                        rvLoadMore.setLoaded()
                        Log.d("TAG", "GET_NEWS: ${it.data}")
                        newsAdapter.setData(it.data)
                        DataStoreManager(requireContext()).totalArticles.asLiveData()
                            .observe(requireActivity()) {
                                totalArticles = it
                                rvLoadMore.setLastPage(
                                    itemsDisplayed + Constant.PAGE_SIZE_QUERY >= (totalArticles
                                        ?: 0)
                                )
                                itemsDisplayed += Constant.PAGE_SIZE_QUERY
                            }
                        newsAdapter.setOnItemClickListener {
                            val bundle = Bundle()
                            bundle.putString("url", it.url)

                            Log.e("TAG", "DATAKU_DETAIL: $bundle")
                            navController.navigate(
                                R.id.action_newsFragment_to_detailFragment,
                                bundle
                            )
                        }
                    }

                    is State.Empty -> {
                        toast("Err")
                    }

                    else -> {}
                }
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun setUpCategoriesRecyclerView() {
        binding.rvCategory.post {
            categoryAdapter.notifyDataSetChanged()
        }
        binding.rvCategory.apply {
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            categoryAdapter.apply {
                adapter = this
            }
        }
    }

    private fun setUpNewsRecyclerView() {
        binding.rvNews.initRecycler(LinearLayoutManager(requireContext()), newsAdapter)
        binding.rvNews.addOnScrollListener(rvLoadMore)
        rvLoadMore.setOnLoadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                viewModel.getNews(rvLoadMore.getCurrentPage(), false, currentCategory)
                binding.progressBar.visibility = View.VISIBLE
            }
        })
    }

    private fun darkMode() {
        binding.switchTheme.setOnCheckedChangeListener { _, isChecked ->
            val theme = if (isChecked) {
                AppCompatDelegate.MODE_NIGHT_YES
            } else {
                AppCompatDelegate.MODE_NIGHT_NO
            }

            if (isChecked) {
                binding.rlTop.setBackgroundColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.black_bg
                    )
                )
                binding.tvAppName.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.white
                    )
                )
            } else {
                binding.rlTop.setBackgroundColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.off_white
                    )
                )
                binding.tvAppName.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.black
                    )
                )
            }

            lifecycleScope.launch {
                if (isChecked)
                    DataStoreManager(requireContext()).saveIsThemeDark(true)
                else
                    DataStoreManager(requireContext()).saveIsThemeDark(false)
            }

            AppCompatDelegate.setDefaultNightMode(theme)
        }
    }

    override fun onResume() {
        super.onResume()

        DataStoreManager(requireContext()).isThemeDark.asLiveData()
            .observe(viewLifecycleOwner) { isThemeDark ->

                //changing switch state using isThemeDark boolean stored in DataStore
                binding.switchTheme.isChecked = isThemeDark

                val theme = if (isThemeDark) {
                    AppCompatDelegate.MODE_NIGHT_YES
                } else {
                    AppCompatDelegate.MODE_NIGHT_NO
                }

                if (isThemeDark) {
                    binding.rlTop.setBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.black_bg
                        )
                    )
                    binding.tvAppName.setTextColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.white
                        )
                    )
                } else {
                    binding.rlTop.setBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.off_white
                        )
                    )
                    binding.tvAppName.setTextColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.black
                        )
                    )
                }

                lifecycleScope.launch {
                    if (isThemeDark)
                        DataStoreManager(requireContext()).saveIsThemeDark(true)
                    else
                        DataStoreManager(requireContext()).saveIsThemeDark(false)
                }
                AppCompatDelegate.setDefaultNightMode(theme)
                categoryAdapter.notifyDataSetChanged()
                newsAdapter.notifyDataSetChanged()
            }
    }

    override fun onCategoryClicked(category: String) {
        currentCategory = category
        itemsDisplayed = 0
        rvLoadMore.setCurrentPage(1)
        viewModel.getNews(1, true, category)
    }
}