package com.skaha.mynews.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.oratakashi.viewbinding.core.binding.livedata.liveData
import com.oratakashi.viewbinding.core.tools.State
import com.skaha.mynews.data.NewsRepository
import com.skaha.mynews.data.model.news.NewsEntity
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class HomeViewModel(
    private val newsRepository: NewsRepository
): ViewModel() {
    private val job: Job by lazy { Job() }
    private val _news : MutableLiveData<State<List<NewsEntity>>> by liveData()
    val news = _news as LiveData<State<List<NewsEntity>>>
    private var allNewsDisplayed = mutableListOf<NewsEntity>()

    fun getNews(page: Int, isNew: Boolean, category: String){
        viewModelScope.launch {
            if (isNew) {
                //if call is new then clearing the previous paginated news
                allNewsDisplayed.clear()
            }
            newsRepository.getNews(page, isNew, category)
                .onStart { _news.postValue(State.loading()) }
                .catch {
                    _news.postValue(State.fail(it, it.message))
                }
                .collect {
                    allNewsDisplayed.addAll(it.map { data ->
                        data
                     })
                    _news.postValue(State.success(allNewsDisplayed.toList()))
                }
        }
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}