package com.skaha.mynews.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.skaha.mynews.data.model.news.NewsEntity
import com.skaha.mynews.databinding.AdapterNewsBinding


class NewsAdapter : RecyclerView.Adapter<NewsAdapter.MyViewHolder>() {

    private var news = emptyList<NewsEntity>()
    lateinit var binding: AdapterNewsBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        binding = AdapterNewsBinding.inflate(inflater, parent, false)
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(news[position])
    }

    override fun getItemCount(): Int = news.size

    inner class MyViewHolder(private val binding: AdapterNewsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(article: NewsEntity) {
            binding.tvTitle.text = article.author
            binding.tvDes.text = article.description
            if (article.source?.name != null)
                binding.tvName.text = article.source.name
            Glide.with(binding.root).load(article.urlToImage)
                .transform(CenterCrop(), RoundedCorners(30)).into(binding.newsImage)
            binding.root.setOnClickListener {
                onItemClickListener?.let {
                    it(article)
                }
            }

        }
    }

    private var onItemClickListener: ((NewsEntity) -> Unit)? = null

    fun setOnItemClickListener(listener: (NewsEntity) -> Unit) {
        onItemClickListener = listener
    }

    fun setData(newData: List<NewsEntity>) {
        val newsDiffUtil = NewsDiffUtil(news, newData)
        val diffUtils = DiffUtil.calculateDiff(newsDiffUtil)
        news = newData
        diffUtils.dispatchUpdatesTo(this)
    }
}