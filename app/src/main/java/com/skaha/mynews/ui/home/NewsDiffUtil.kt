package com.skaha.mynews.ui.home

import androidx.recyclerview.widget.DiffUtil
import com.skaha.mynews.data.model.news.NewsEntity

class NewsDiffUtil(
    private val oldlist: List<NewsEntity>,
    private val newlist: List<NewsEntity>
) : DiffUtil.Callback() {
    override fun getOldListSize(): Int = oldlist.size

    override fun getNewListSize(): Int = newlist.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldlist[oldItemPosition] === newlist[newItemPosition]
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {

        return oldlist[oldItemPosition] === newlist[newItemPosition]
    }
}