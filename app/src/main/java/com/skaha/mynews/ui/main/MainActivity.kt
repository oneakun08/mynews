package com.skaha.mynews.ui.main

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.asLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import com.oratakashi.viewbinding.core.binding.activity.viewBinding
import com.oratakashi.viewbinding.core.tools.State
import com.oratakashi.viewbinding.core.tools.toast
import com.skaha.mynews.databinding.ActivityMainBinding
import com.skaha.mynews.utils.Constant
import com.skaha.mynews.utils.DataStoreManager
import com.skaha.mynews.utils.OnLoadMoreListener
import com.skaha.mynews.utils.RVLoadMore
import com.skaha.mynews.utils.extention.initRecycler
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity() {

    private val binding: ActivityMainBinding by viewBinding()
//    private val viewModel: MainViewModel by viewModel()
//    private val newsAdapter: NewsAdapter by lazy {
//        NewsAdapter()
//    }
//    private var itemsDisplayed = 0
//    private var totalArticles: Int? = null
//    private var rvLoadMore: RVLoadMore = RVLoadMore()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.apply {
            // Do something here
//            setupRecyclerView()
//            initAction()
//            initObserver()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.apply {
            this@MainActivity.isDestroyed
        }
    }

//    private fun initAction() {
//        viewModel.getNews(1, true)
//    }
//
//    private fun initObserver() {
//        with(binding) {
//            viewModel.news.observe(this@MainActivity) {
//                when (it) {
//                    is State.Loading -> {
//                        // Do something here
//                    }
//
//                    is State.Success -> {
//                        progressBar.visibility = View.GONE
//                        rvLoadMore.setLoaded()
//                        // Do something here
//                        Log.d("TAG", "GET_NEWS: ${it.data}")
//                        newsAdapter.setData(it.data)
//                        DataStoreManager(this@MainActivity).totalArticles.asLiveData()
//                            .observe(this@MainActivity) {
//                                totalArticles = it
//                                rvLoadMore.setLastPage(
//                                    itemsDisplayed + Constant.PAGE_SIZE_QUERY >= (totalArticles
//                                        ?: 0)
//                                )
//                                itemsDisplayed += Constant.PAGE_SIZE_QUERY
//                            }
//                        newsAdapter.setOnItemClickListener {
//
//                        }
//                    }
//
//                    is State.Empty -> {
//                        // Do something here
//                        toast("Err")
//                    }
//
//                    else -> {}
//                }
//            }
//        }
//    }
//
//    private fun setupRecyclerView() {
//        binding.rvNews.initRecycler(LinearLayoutManager(this@MainActivity), newsAdapter)
//        binding.rvNews.addOnScrollListener(rvLoadMore)
//        rvLoadMore.setOnLoadMoreListener(object : OnLoadMoreListener {
//            override fun onLoadMore() {
//                viewModel.getNews(rvLoadMore.getCurrentPage(), false)
//                binding.progressBar.visibility = View.VISIBLE
//            }
//        })
//    }
}