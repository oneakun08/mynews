package com.skaha.mynews.utils

object Constant {
    const val PAGE_SIZE_QUERY = 20
    const val DATASTORE = "datastore"
}