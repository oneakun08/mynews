package com.skaha.mynews.utils

interface OnLoadMoreListener {
    fun onLoadMore()
}