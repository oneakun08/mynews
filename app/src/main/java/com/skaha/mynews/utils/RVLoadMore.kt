package com.skaha.mynews.utils

import android.widget.AbsListView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class RVLoadMore : RecyclerView.OnScrollListener() {

    private var currentPage = 1
    private var isScrolling: Boolean = false
    private var isLastPage: Boolean = false
    private var isLoading: Boolean = false
    private lateinit var mOnLoadMoreListener: OnLoadMoreListener

    fun setLoaded() {
        isLoading = false
    }
    fun getLoaded(): Boolean {
        return isLoading
    }

    fun setCurrentPage(page: Int) {
        currentPage = page
    }

    fun getCurrentPage(): Int {
        return currentPage
    }

    fun setLastPage(lastPage: Boolean) {
        isLastPage = lastPage
    }

    fun getLastPage(): Boolean {
        return isLastPage
    }

    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
            isScrolling = true
        }
    }
    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        //getting the linear layout manager from recycler view
        val layoutManager = recyclerView.layoutManager as LinearLayoutManager

        //now getting the first item position which is visible on page
        val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()

        //getting the total visible items count on page
        val visibleItemCount = layoutManager.childCount

        //getting the total items count in recycler view
        val totalItemsCount = layoutManager.itemCount

        //checking if user is not at the last page and is not loading news items
        val isNotAtLastPageAndNotLoading = !getLastPage() && !getLoaded()

        //checking if user is at the last page of the response
        val isAtLastPage = firstVisibleItemPosition + visibleItemCount >= totalItemsCount

        //checking if user has scrolled from the first page
        val notAtBeginning = firstVisibleItemPosition >= 0

        //checking if the no.of items in 1 query response are all loaded in recycler view
        val isTotalMoreThanVisible = totalItemsCount >= Constant.PAGE_SIZE_QUERY

        //creating a boolean to know if to paginate or not
        val shouldPaginate =
            isNotAtLastPageAndNotLoading && isAtLastPage && isTotalMoreThanVisible && notAtBeginning && isScrolling
        if (shouldPaginate) {
            currentPage += 1
            setCurrentPage(currentPage)
            mOnLoadMoreListener.onLoadMore()
            isScrolling = false
        }
    }

    fun setOnLoadMoreListener(mOnLoadMoreListener: OnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener
    }
}